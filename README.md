# The Pixelmon Mod Percy's Pack

Added automation mods to the pixelmon modpack


# Known issues 

* The tesseract doesn't interact directly with AE2 importers/exporters and Buildcraft pipes


# Present mods

* Applied Energistics 2 + extension
* Agricraft
* Biomes O'Plenty
* Buildcraft
* CraftTweaker2
* Ender IO
* Extra Utilities 2
* Falling Tree
* Fancy Menu
* Fast Furnaces
* Gameshark
* Industrial Foregoing
* Integration Foregoing
* Inventory Tweaks
* Iron Backpacks
* JourneyMap
* JEI + NEI
* Mouse Tweaks
* Nature's Compass
* Optifine
* Pixelmon, Pixel extras
* Tesseract
* Thermal Cultivation, Dynamics, Expansions, Foundation, Innovation
* Trash Cans
* Unidict
* Chicken Chunks
* Hwla + Waila Harvestability

# To-Do

* Change Night/Day Cycle
* Find a good Shader (AE2 is currently bugged with RT Shaders)
* Change Chunk Loader Recipe or make Ender Pearl custom recipe (Enderman not spawning)
* ~~Fix Lead ore rarity~~


# Notes 

* 8GB+ of RAM is recommended
* RT Shaders are not recommended
* Some mods hotkeys conflict, please change them to your liking (Es. R)

#LICENSE

See License file for more informations
